#pragma once 
#include <iostream> 
#include <string> 
#include <map> 
#include <vector> 
#include <fstream> 
#include <sstream> 
#include "item.h" 

//Room class

class Room {
private:
	std::string description; //hold the description of the room
	std::map<std::string, Room*> exits; //Map used to store exits using room name as keys and Room pointers as values
	std::vector<Item> items; //contains the room inventory of items 
public:
	void PrintConnections(); //print the connections for a room
	Room(std::string desc); //A constructor for the Room class
	void AddItem(Item& item); // add items to a room
	std::string GetDescription(); // get the description of a room
	std::vector<Item> GetItems(); // get the items from a room
	std::map<std::string, Room*>& GetExits(); //get the exits for a room
	Room* GetExit(std::string direction); // get an exit from a room with the direction
	void connectExit(std::string direction, Room* room); //connect the exits from the current room to the new room
};
