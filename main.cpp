#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <random>
#include <chrono>
#include <thread>
#include <ctime> 
#include <cstdlib> 
#include "item.h"
#include "area.h"
#include "character.h"
#include "room.h"

int main() {
    // Create an instance of the Area class
    Area gameWorld;
    // Load the game map from a text file
    gameWorld.LoadMapFromFile("C:/Users/User/OneDrive/cs year 1/programming in C/spring term/1/PCSCW/PCSCW/game_map.txt");
    // Create a Player
    Player player("Alice", 100);
    // Set the player's starting room (you can modify this room name)
    Room* currentRoom = gameWorld.GetRoom("startRoom");
    player.SetLocation(currentRoom);

    int roomTransitions = 0; // Counter for room transitions
    int bossHealth = 0; // Boss's health
    bool hasDefeatedPat = false; // Track whether the player has defeated Wizard Pat

    // Game loop
    while (true) {
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
        std::cout << "Items in the room:" << std::endl;
        for (Item& item : player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }
        std::cout << "Exits available: ";
        for (auto& exit : player.GetLocation()->GetExits()) {
            std::cout << exit.first << " ";
        }
        std::cout << "| ";
        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Quit" << std::endl;
        int choice;
        std::cin >> choice;

        if (choice == 1) {
            std::cout << "You look around the room." << std::endl;
        }
        else if (choice == 2) {
            // Player interacts with an item in the room
            std::cout << "Enter the name of the item you want to interact with:";
            std::string itemName;
            std::cin >> itemName;
            for (Item& item : player.GetLocation()->GetItems()) {
                if (item.GetName() == itemName) {
                    item.Interact();
                    break;
                }
            }
        }
        else if (choice == 3) {
            // Player moves to another room
            std::cout << "Enter the direction (e.g., north, south): ";
            std::string direction;
            std::cin >> direction;
            Room* nextRoom = player.GetLocation()->GetExit(direction);
            if (nextRoom != nullptr) {
                player.SetLocation(nextRoom);
                std::cout << "You move to the next room." << std::endl;
                currentRoom = nextRoom;
                roomTransitions++;

                // Check if it's the third room transition
                if (roomTransitions == 3 && !hasDefeatedPat) {
                    // Generate random boss health between 1 and 6
                    std::srand(std::time(nullptr)); // Seed the random number generator
                    bossHealth = std::rand() % 6 + 1;
                    std::cout << "You encountered Wizard Pat with " << bossHealth << " health! Press 'f' to fight!" << std::endl;

                    // Player has limited time to fight the boss
                    std::cout << "You have 5 seconds to press 'f' to fight..." << std::endl;
                    std::this_thread::sleep_for(std::chrono::seconds(5));

                    // Check if the player fought the boss
                    char fightChoice;
                    std::cin >> fightChoice;

                    // Clear the input buffer to avoid unexpected behavior
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

                    if (fightChoice == 'f') {
                        std::cout << "You fought Wizard Pat!" << std::endl;
                        // Update boss health based on player's attack (not implemented)
                        // Print the outcome of the fight (not implemented)

                        // Set the flag indicating that the player has defeated Wizard Pat
                        hasDefeatedPat = true;
                    }
                    else {
                        std::cout << "You didn't fight Wizard Pat in time. You lose!" << std::endl;
                        // Handle losing scenario (not implemented)
                        break; // End the game loop since the player has lost
                    }
                }
            }
            else {
                std::cout << "You can't go that way." << std::endl;
            }
        }
        else if (choice == 4) {
            // Quit the game
            std::cout << "Goodbye!" << std::endl;
            break;
        }
        else {
            std::cout << "Invalid choice. Try again." << std::endl;
        }
    }

    return 0;
}

