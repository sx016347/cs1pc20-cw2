#include <iostream> 
#include <string> 
#include <map> 
#include <vector> 
#include <fstream> 
#include <sstream>
#include "room.h" 
#include "item.h" 

//Room class
Room::Room(std::string desc) : description(desc) {}; //Constructor for room class, intitializes member variable description with value passed as desc 
void Room::PrintConnections() { //PrintConnections assigned to Room
    std::cout << "\nRoom: " << description << " & ";
    for (auto& exit : exits) { //Parses through the available exits
        std::cout << "Exit: " << exit.first << ", Connected Room: " << exit.second->GetDescription() << "\n" << std::endl;
        //Outputs the direction and description of connected room
    }
}
void Room::AddItem(Item& item) { 
    items.emplace_back(item); //Adds the item back to the room
};
std::string Room::GetDescription() { 
    return description; //Returns description
}
std::vector<Item> Room::GetItems() { 
    return items; //Returns items
}
std::map<std::string, Room*>& Room::GetExits() { //Function to get the exits from the room
    return exits; //Returns the available exits
}
Room* Room::GetExit(std::string direction) { //
    auto it = exits.find(direction); //Search for the exit in the exits map based on the given direction
    std::cout << "\n" << direction; //Outputs the direction being searched for
    if (it != exits.end()) { //Checks if the exit was found
        return it->second; //Returns a pointer to the connected room
    }
    else {
        std::cout << "\nExit not found for direction " << direction << std::endl; //Outputs the exit wasn't found
        return nullptr; //Returns a null pointer
    }
}
void Room::connectExit(std::string direction, Room* room) {
    exits[direction] = room; //Connects the exit with the direction to the room
}
