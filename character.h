#pragma once 
#include <iostream> 
#include <string> 
#include <vector> 
#include "item.h" 
#include "room.h" 

//Character class and Player class

/*
//New features are hitpoints and experience
*/
class Character {
private:
	std::string name; //character name
	int health; //Integer to give the healthpoints (new)
	std::vector<Item> inventory; //A vector which contains the character inventory of items
public:
	Character(std::string& name, int health); //Constructor for character class
	void PrintStats(); //output the chracter stats (health, shield hitpoints, level, and XP) 
	void TakeDamage(int damage); // decrease character health
	void AddToInventory(Item& newItem); //add items to the characters inventory
	std::string& GetName(); //get the character name
	std::vector<Item>& GetInventory(); //characters inventory.
};
class Player : public Character {
private:
	Room* location; //player location
public:
	Player(std::string name, int health); //Constructor for Player class
	Room* GetLocation(); //get the players location
	void SetLocation(Room* newLocation); //set the new location for the player
};
