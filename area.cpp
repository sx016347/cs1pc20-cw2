#include <iostream> 
#include <string>
#include <map> 
#include <vector> 
#include <fstream> 
#include <sstream> 
#include "room.h" 
#include "area.h" 

//Area class
void Area::AddRoom(std::string name, Room* room) { 
    rooms[name] = room; //Adds the room to the game
}
Room* Area::GetRoom(std::string name) { // get rooms
    auto it = rooms.find(name); //Finds the room in 'rooms' map 
    return (it != rooms.end()) ? it->second : nullptr; 
}
void Area::ConnectRooms(std::string room1Name, std::string room2Name, std::string direction) {
    Room* room1 = GetRoom(room1Name); //room1 assigned to Room 
    Room* room2 = GetRoom(room2Name); //room2 assigned to Room
    if (room1 && room2) { //Checks if room1 and room2 exists
        room1->connectExit(direction, room2); //Connects the exit for room1
        room2->connectExit(direction, room1); //Connects the exit for room2 
    }
    else {
        std::cerr << "No connection available" << std::endl; //Outputs no connection available
    }
}
void Area::LoadMapFromFile(std::string filename) { //loads the game files
    std::ifstream file(filename); //Opens the file for reading
    if (!file.is_open()) { //Checks if the file is open
        std::cerr << "Error opening file: " << filename << std::endl; //Outputs a message saying theres an error opening the file
        return; 
    }
    std::string line; 
    Room* startingRoom = nullptr;  // Initialize startingRoom outside the loop
    std::map<std::string, Room*> rooms;  // Temporary map to store rooms
    while (std::getline(file, line)) { //Read each line from the file
        std::istringstream iss(line); //Create a string stream to parse the line
        std::string roomName, description, command, connectedRoomName, direction; //stores room name, description and connections
        // Extract room information
        std::getline(iss, roomName, '|');
        if (rooms.count(roomName) == 0) { //Check if room has already been created
            std::getline(iss, description, '|');
           
            Room* newRoom = new Room(description);
            rooms[roomName] = newRoom;
            if (startingRoom == nullptr) { //Set starting room if not set
                startingRoom = newRoom;
            }
        }
        // Extract connections
        while (iss >> command) {
            if (command == "Connect") {
                iss >> connectedRoomName >> direction; //Read connected room name and direction
                rooms[roomName]->connectExit(direction, rooms[connectedRoomName]); //Connect room with connected room
            }
        }
    }
    file.close(); //Closes the file
    this->startingRoom = startingRoom;
    this->rooms = rooms;
}
Room* Area::GetStartingRoom() { //A function to get the starting room
    return startingRoom; //Returns the startingRoom
}
